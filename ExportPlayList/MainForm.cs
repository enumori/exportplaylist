﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Threading.Tasks;

namespace ExportPlayList
{
    public partial class MainForm : Form
    {
        public class t_containment
        {
            public int ParentId { set; get; }
            public int ChildId { set; get; }
            public int Order { set; get; }
        }

        public class t_object
        {
            public int ObjectId { set; get; }
            public int ExportID1 { set; get; }
            public int ExportID2 { set; get; }
            public string ObjectName { set; get; }
            public int Length { set; get; }
            public string Path { set; get; }
        }

        private List<t_containment> _ContainmentList = new List<t_containment>();
        private List<t_object> _ObjectList = new List<t_object>();

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(Properties.Settings.Default.DebugFile))
            {
                Microsoft.Win32.RegistryView registryView;

                if (Properties.Settings.Default.Is32Bit)
                {
                    registryView = Microsoft.Win32.RegistryView.Registry32;
                }
                else
                {
                    registryView = Microsoft.Win32.RegistryView.Registry64;
                }
                using (var base_key = Microsoft.Win32.RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, registryView))
                {
                    var sub_key = base_key.OpenSubKey(Properties.Settings.Default.RegistryKey);
                    _MetallicData.Text = (string)sub_key.GetValue("MetallicData");
                    sub_key.Close();
                }
            }
            else
            {
                _MetallicData.Text = Properties.Settings.Default.DebugFile;
            }
            _FolderBrowserDialog.SelectedPath = System.Environment.GetFolderPath(Environment.SpecialFolder.Personal);
        }

        private void _Open_Click(object sender, EventArgs e)
        {
            if (_FolderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                if (System.IO.Directory.Exists(_FolderBrowserDialog.SelectedPath))
                {
                    _ExportFolder.Text = _FolderBrowserDialog.SelectedPath;
                }
            }
        }

        private void _Export_Click(object sender, EventArgs e)
        {
            if (!System.IO.Directory.Exists(_ExportFolder.Text))
            {
                _ToolStripStatusLabel.Text = @"出力先フォルダを設定してください。";
                return;
            }
            if (!System.IO.File.Exists(_MetallicData.Text))
            {
                _ToolStripStatusLabel.Text = @"データベースファイルが見つかりません。";
                return;
            }

            _ContainmentList.Clear();
            _ObjectList.Clear();

            ReadDatabse();

            ExportPlayList();
        }

        private void ReadDatabse()
        {
            _ToolStripStatusLabel.Text = @"データベースの読み込み開始。";
            OleDbConnection conn = new OleDbConnection();
            conn.ConnectionString = string.Format(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0}", _MetallicData.Text);
            try
            {
                conn.Open();
                DataTable table = conn.GetSchema("Tables", new string[4] { null, null, null, "TABLE" });

                if (!table.AsEnumerable().Where(w => w["TABLE_NAME"].ToString() == Properties.Settings.Default.ContainmentTable).Any())
                {
                    _ToolStripStatusLabel.Text = String.Format("テーブル：{0}が見つかりません。", Properties.Settings.Default.ContainmentTable);
                    return;
                }
                if (!table.AsEnumerable().Where(w => w["TABLE_NAME"].ToString() == Properties.Settings.Default.ObjectTable).Any())
                {
                    _ToolStripStatusLabel.Text = String.Format("テーブル：{0}が見つかりません。", Properties.Settings.Default.ObjectTable);
                    return;
                }

                using (OleDbCommand cmd = new OleDbCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "SELECT * FROM " + Properties.Settings.Default.ContainmentTable + ";";
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                int ParentId = (int)reader["ParentId"];
                                int ChildId = (int)reader["ChildId"];
                                int Order = (int)reader["Order"];
                                _ContainmentList.Add(new t_containment() { ParentId = ParentId, ChildId = ChildId, Order = Order });
                            }

                        }
                    }
                }

                using (OleDbCommand cmd = new OleDbCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "SELECT * FROM " + Properties.Settings.Default.ObjectTable + ";";
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                int ObjectId = (int)reader["ObjectId"];
                                int ExportID1 = (int)reader["ObjectSpecId"];
                                int ExportID2 = (int)reader["101"];
                                string ObjectName = DBNull.Value.Equals(reader["ObjectName"]) ? "" : (string)reader["ObjectName"];
                                int Length = (int)reader["100"];
                                string Path= DBNull.Value.Equals(reader["500"]) ? "" : (string)reader["500"];
                                _ObjectList.Add(new t_object() { ObjectId = ObjectId, ExportID1 = ExportID1, ExportID2 = ExportID2, ObjectName = ObjectName, Length = Length, Path = Path});
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _ToolStripStatusLabel.Text = ex.Message;
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }
        }

        private void ExportPlayList()
        {
            _ToolStripStatusLabel.Text = @"プレイリストの書き出し開始。";
            _Export.Enabled = false;
            var task = Task.Factory.StartNew(() =>
            {
                var lists = _ObjectList.Where(w => w.ExportID1 == Properties.Settings.Default.ExportID1 && w.ExportID2 == Properties.Settings.Default.ExportID2).Select(s => s).ToList();
                foreach (var list in lists)
                {
                    var file = list.ObjectName + Properties.Settings.Default.Extension;
                    using (System.IO.StreamWriter sr = new System.IO.StreamWriter(System.IO.Path.Combine(_ExportFolder.Text, file), false, System.Text.Encoding.GetEncoding("utf-8")))
                    {
                        _ToolStripStatusLabel.Text = @"プレイリスト：" + list.ObjectName + "書き出し開始。";
                        sr.WriteLine("#EXTM3U");
                        var items = _ContainmentList.Where(w => w.ParentId == list.ObjectId).OrderBy(o => o.Order).Select(s => s).ToList();
                        foreach (var item in items)
                        {
                            var info = _ObjectList.Where(w => w.ObjectId == item.ChildId).Select(s => s).FirstOrDefault();
                            sr.WriteLine(String.Format("#EXTINF:{0},{1}", info.Length, info.ObjectName));
                            sr.WriteLine(info.Path);
                        }
                    }
                }
                return;
            });
            task.ContinueWith(t =>
            {
                _Export.Enabled = true;
                _ToolStripStatusLabel.Text = @"終了";
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }
    }
}
