﻿namespace ExportPlayList
{
    partial class MainForm
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this._MetallicData = new System.Windows.Forms.TextBox();
            this._StatusStrip = new System.Windows.Forms.StatusStrip();
            this._ToolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.label2 = new System.Windows.Forms.Label();
            this._ExportFolder = new System.Windows.Forms.TextBox();
            this._Open = new System.Windows.Forms.Button();
            this._Export = new System.Windows.Forms.Button();
            this._FolderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this._StatusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "MetallicData";
            // 
            // _MetallicData
            // 
            this._MetallicData.Location = new System.Drawing.Point(122, 10);
            this._MetallicData.Name = "_MetallicData";
            this._MetallicData.ReadOnly = true;
            this._MetallicData.Size = new System.Drawing.Size(450, 19);
            this._MetallicData.TabIndex = 1;
            // 
            // _StatusStrip
            // 
            this._StatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._ToolStripStatusLabel});
            this._StatusStrip.Location = new System.Drawing.Point(0, 159);
            this._StatusStrip.Name = "_StatusStrip";
            this._StatusStrip.Size = new System.Drawing.Size(584, 22);
            this._StatusStrip.TabIndex = 2;
            this._StatusStrip.Text = "statusStrip1";
            // 
            // _ToolStripStatusLabel
            // 
            this._ToolStripStatusLabel.Name = "_ToolStripStatusLabel";
            this._ToolStripStatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "書き出し先フォルダ";
            // 
            // _ExportFolder
            // 
            this._ExportFolder.Location = new System.Drawing.Point(122, 42);
            this._ExportFolder.Name = "_ExportFolder";
            this._ExportFolder.Size = new System.Drawing.Size(370, 19);
            this._ExportFolder.TabIndex = 1;
            // 
            // _Open
            // 
            this._Open.Location = new System.Drawing.Point(497, 40);
            this._Open.Name = "_Open";
            this._Open.Size = new System.Drawing.Size(75, 23);
            this._Open.TabIndex = 4;
            this._Open.Text = "開く";
            this._Open.UseVisualStyleBackColor = true;
            this._Open.Click += new System.EventHandler(this._Open_Click);
            // 
            // _Export
            // 
            this._Export.Location = new System.Drawing.Point(13, 75);
            this._Export.Name = "_Export";
            this._Export.Size = new System.Drawing.Size(559, 70);
            this._Export.TabIndex = 5;
            this._Export.Text = "エクスポート";
            this._Export.UseVisualStyleBackColor = true;
            this._Export.Click += new System.EventHandler(this._Export_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 181);
            this.Controls.Add(this._Export);
            this.Controls.Add(this._Open);
            this.Controls.Add(this.label2);
            this.Controls.Add(this._StatusStrip);
            this.Controls.Add(this._ExportFolder);
            this.Controls.Add(this._MetallicData);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "MainForm";
            this.Text = "MusicCenterForPCのPlayListをexport";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this._StatusStrip.ResumeLayout(false);
            this._StatusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox _MetallicData;
        private System.Windows.Forms.StatusStrip _StatusStrip;
        private System.Windows.Forms.ToolStripStatusLabel _ToolStripStatusLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox _ExportFolder;
        private System.Windows.Forms.Button _Open;
        private System.Windows.Forms.Button _Export;
        private System.Windows.Forms.FolderBrowserDialog _FolderBrowserDialog;
    }
}

